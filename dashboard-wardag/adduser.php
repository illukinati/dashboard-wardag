<?php
  include './php/connect.php';
?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Warung Daging</title>


  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />


  <!-- Bootstrap core CSS     -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/bootstrap-4.1.1/dist/css/bootstrap-grid.min.css" rel="stylesheet" />

  <link href="./css/addproduct.css" rel="stylesheet">
  <!--  Light Bootstrap Dashboard core CSS    -->
  <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

  <!--  CSS for Demo Purpose, don't include it in your project     -->
  <link href="../assets/css/demo.css" rel="stylesheet" />


  <!--     Fonts and icons     -->
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>

<body>

  <div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="../assets/img/full-screen-image-3.jpg">
      <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

      <div class="logo">
        <a href="http://www.creative-tim.com" class="logo-text">
          Creative Tim
        </a>
      </div>
      <div class="logo logo-mini">
        <a href="http://www.creative-tim.com" class="logo-text">
          Ct
        </a>
      </div>

      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="../assets/img/default-avatar.png" />
          </div>
          <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              Tania Andrew
              <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li>
                  <a href="#">My Profile</a>
                </li>
                <li>
                  <a href="#">Edit Profile</a>
                </li>
                <li>
                  <a href="#">Settings</a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <ul class="nav">
          <li>
            <a href="../new_dashboard.php">
              <i class="pe-7s-graph"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="./products.php">
              <i class="pe-7s-plugin"></i>
              <p>Products
              </p>
            </a>
          </li>

          <li class="active">
            <a href="./user.php">
              <i class="pe-7s-user"></i>
              <p>User
              </p>
            </a>
          </li>
          <li>
            <a href="./order.php">
              <i class="pe-7s-cart"></i>
              <p>Order
              </p>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
              <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
              <i class="fa fa-navicon visible-on-sidebar-mini"></i>
            </button>
          </div>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Dashboard PRO</a>
          </div>
          <div class="collapse navbar-collapse">

            <form class="navbar-form navbar-left navbar-search-form" role="search">
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-search"></i>
                </span>
                <input type="text" value="" class="form-control" placeholder="Search...">
              </div>
            </form>

            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="charts.html">
                  <i class="fa fa-line-chart"></i>
                  <p>Stats</p>
                </a>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-gavel"></i>
                  <p class="hidden-md hidden-lg">
                    Actions
                    <b class="caret"></b>
                  </p>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Create New Post</a>
                  </li>
                  <li>
                    <a href="#">Manage Something</a>
                  </li>
                  <li>
                    <a href="#">Do Nothing</a>
                  </li>
                  <li>
                    <a href="#">Submit to live</a>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <a href="#">Another Action</a>
                  </li>
                </ul>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="notification">5</span>
                  <p class="hidden-md hidden-lg">
                    Notifications
                    <b class="caret"></b>
                  </p>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Notification 1</a>
                  </li>
                  <li>
                    <a href="#">Notification 2</a>
                  </li>
                  <li>
                    <a href="#">Notification 3</a>
                  </li>
                  <li>
                    <a href="#">Notification 4</a>
                  </li>
                  <li>
                    <a href="#">Another notification</a>
                  </li>
                </ul>
              </li>

              <li class="dropdown dropdown-with-icons">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-list"></i>
                  <p class="hidden-md hidden-lg">
                    More
                    <b class="caret"></b>
                  </p>
                </a>
                <ul class="dropdown-menu dropdown-with-icons">
                  <li>
                    <a href="#">
                      <i class="pe-7s-mail"></i> Messages
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="pe-7s-help1"></i> Help Center
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="pe-7s-tools"></i> Settings
                    </a>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <a href="#">
                      <i class="pe-7s-lock"></i> Lock Screen
                    </a>
                  </li>
                  <li>
                    <a href="#" class="text-danger">
                      <i class="pe-7s-close-circle"></i>
                      Log out
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="row">
              <div class="col-lg-12 col-md-4 col-sm-2">
                <div class="content">
                  <!--INPUT FORM TO ADD PRODUCT DATA-->
                  <form method="POST" action="">
                    <div class="row">  
                      <div class="col-lg-6 col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>NAMA LENGKAP</label>
                          <input name="nama" type="text" placeholder="Nama" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-6 col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>NAMA PENGGUNA</label>
                          <input name="username" type="text" placeholder="okuzapru" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                          <label>NO TELEPON</label>
                          <input name="nomor_hp" placeholder="08123456789" type="tel" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                          <label>ALAMAT</label>
                          <input name="alamat" placeholder="Jalan Simpang" type="text" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                          <label>EMAIL</label>
                          <input name="email" placeholder="roberto@gmail.com" type="email" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                          <label>PASSWORD</label>
                          <input type="password" name="pwd" placeholder="Strong password" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                          <label>ROLE</label>
                          <div class="row">
                            <div class="col-lg-6">
                              <label class="radio">
                                <input name="role" type="radio" data-toggle="radio" value="User" checked>User
                              </label>
                            </div>
                            <div class="col-lg-6">
                              <label class="radio">
                                <input name="role" type="radio" data-toggle="radio" value="Mitra">Mitra
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button name="submit" type="submit" class="center-block btn btn-warning btn-fill btn-wd">ADD USER</button>
                  </form>
                </div>
              </div>
            </div>

          </div>
          <!--  end card  -->

          <!-- end col-md-12 -->

          <!-- end row -->

        </div>
      </div>

      <footer class="footer">
        <div class="container-fluid">
          <nav class="pull-left">

          </nav>
          <p class="copyright pull-right">
            &copy; 2018
            <a href="#">Creative Tim</a>, dibuat dengan Semangat Pemuda
          </p>
        </div>
      </footer>

    </div>
  </div>
  <?php
    if(isset($_POST['submit'])){
      $nama = $_POST['nama'];
      $namaPengguna = $_POST['username'];      
      $nomorHp = $_POST['nomor_hp'];
      $alamat = $_POST['alamat'];
      $role = $_POST['role'];
      $email = $_POST['email'];
      $pwd = $_POST['pwd'];
      
      $created_at = date("Y-m-d H:i:s");
      
      // $uploaddir = 'uploads/';
      // $uploadfile = $uploaddir . basename($_FILES['gambar']['name']);
      
      // if (move_uploaded_file($_FILES['gambar']['tmp_name'], $uploadfile)) {
      //     echo "File is valid, and was successfully uploaded.\n";
      // } else {
      //     echo "Possible file upload attack!\n";
      // }

      // $query = "INSERT INTO users (nama, username, email, phone, alamat, peran, created_at) 
      //           VALUES ('$nama', '$namaPengguna', '$email', '$nomorHp', '$alamat', '$role', '$created_at')";
      $hashedPwd = password_hash($pwd, PASSWORD_BCRYPT, ['cost' => 12]);
      // replace $2y$ to $2a$ or $2b$ so bcrypt in nodejs can decrypt it
      $hashedPwd = str_replace("$2y$", "$2a$", $hashedPwd);


      $query = "INSERT INTO users (username, email, `password`, created_at) 
                VALUES ('$namaPengguna', '$email', '$hashedPwd', '$created_at')";
      
      if (mysqli_query($conn, $query)) {
        $user_id = mysqli_insert_id($conn);
        $query = "INSERT INTO `customers` (`id`, `name`, `phone_number`, `address`, `created_at`, `updated_at`)
        VALUES ('$user_id', '$nama', '$nomorHp', '$alamat', '$created_at', '$created_at');";
        if (mysqli_query($conn, $query)) {
          // check if account is 'Mitra'
          if ($role == 'Mitra') {
            $query = "INSERT INTO `partners` (`name`, `address`, `latitude`, `longitude`, `user_id`, `created_at`, `updated_at`, `nik`, `status`)
            VALUES ('$nama', '$alamat', 0, 0, '$user_id', 'NOW()', 'NOW()', NULL, 'PENDING');";

            if (mysqli_query($conn, $query)) {
              echo "<script>alert('Berhasil input')</script>";
              // redirecting causing insert mitra does not executed
              // echo "<script>window.location.href = './user.php';</script>";
            } else {
              echo "<script>alert('Gagal input')</script>";        
              echo "Error: <br>" . mysqli_error($conn);
            }
          } else {
            echo "<script>alert('Berhasil input')</script>";
            // redirecting causing insert mitra does not executed
            // echo "<script>window.location.href = './user.php';</script>";
          }

        } else {
          echo "<script>alert('Gagal input')</script>";        
          echo "Error: <br>" . mysqli_error($conn);
        }
      } else {
        echo "<script>alert('Gagal input')</script>";        
        echo "Error: <br>" . mysqli_error($conn);
      }
      
    }
  
    
  ?>

</body>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="../assets/js/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="../assets/js/jquery.validate.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="../assets/js/moment.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="../assets/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="../assets/js/bootstrap-selectpicker.js"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="../assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="../assets/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="../assets/js/jquery-jvectormap.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
<script src="../assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="../assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="../assets/js/jquery.datatables.js"></script>


<!--  Full Calendar Plugin    -->
<script src="../assets/js/fullcalendar.min.js"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="../assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>

<script type="text/javascript">
  $(document).ready(function () {

    demo.initDashboardPageCharts();
    demo.initVectorMap();

    $.notify({
      icon: 'pe-7s-bell',
      message: "<b>Light Bootstrap Dashboard PRO</b> - forget about boring dashboards."

    }, {
      type: 'warning',
      timer: 4000
    });


  });
  var $table = $('#bootstrap-table');

  function operateFormatter(value, row, index) {
    return [
      '<a rel="tooltip" title="View" class="btn btn-simple btn-info btn-icon table-action view" href="javascript:void(0)">',
      '<i class="fa fa-image"></i>',
      '</a>',
      '<a rel="tooltip" title="Edit" class="btn btn-simple btn-warning btn-icon table-action edit" href="javascript:void(0)">',
      '<i class="fa fa-edit"></i>',
      '</a>',
      '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)">',
      '<i class="fa fa-remove"></i>',
      '</a>'
    ].join('');
  }

  $().ready(function () {
    window.operateEvents = {
      'click .view': function (e, value, row, index) {
        info = JSON.stringify(row);

        swal('You click view icon, row: ', info);
        console.log(info);
      },
      'click .edit': function (e, value, row, index) {
        info = JSON.stringify(row);

        swal('You click edit icon, row: ', info);
        console.log(info);
      },
      'click .remove': function (e, value, row, index) {
        console.log(row);
        $table.bootstrapTable('remove', {
          field: 'id',
          values: [row.id]
        });
      }
    };

    $table.bootstrapTable({
      toolbar: ".toolbar",
      clickToSelect: true,
      showRefresh: true,
      search: true,
      showToggle: true,
      showColumns: true,
      pagination: true,
      searchAlign: 'left',
      pageSize: 8,
      clickToSelect: false,
      pageList: [8, 10, 25, 50, 100],

      formatShowingRows: function (pageFrom, pageTo, totalRows) {
        //do nothing here, we don't want to show the text "showing x of y from..."
      },
      formatRecordsPerPage: function (pageNumber) {
        return pageNumber + " rows visible";
      },
      icons: {
        refresh: 'fa fa-refresh',
        toggle: 'fa fa-th-list',
        columns: 'fa fa-columns',
        detailOpen: 'fa fa-plus-circle',
        detailClose: 'fa fa-minus-circle'
      }
    });

    //activate the tooltips after the data table is initialized
    $('[rel="tooltip"]').tooltip();

    $(window).resize(function () {
      $table.bootstrapTable('resetView');
    });


  });
</script>

</html>