<?php
  include './php/connect.php';

  $query = "SELECT * 
            FROM orders AS o INNER JOIN order_items AS ot ON o.id = ot.order_id
            INNER JOIN products AS p ON ot.product_id = p.id INNER JOIN customers AS c
            ON o.customer_id = c.id";
  $res = mysqli_query($conn, $query)

?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Warung Daging</title>


  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />


  <!-- Bootstrap core CSS     -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

  <!--  Light Bootstrap Dashboard core CSS    -->
  <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

  <!--  CSS for Demo Purpose, don't include it in your project     -->
  <link href="../assets/css/demo.css" rel="stylesheet" />


  <!--     Fonts and icons     -->
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>

<body>

  <div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="../assets/img/full-screen-image-3.jpg">
      <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

      <div class="logo">
        <a href="http://www.creative-tim.com" class="logo-text">
          Creative Tim
        </a>
      </div>
      <div class="logo logo-mini">
        <a href="http://www.creative-tim.com" class="logo-text">
          Ct
        </a>
      </div>

      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="../assets/img/default-avatar.png" />
          </div>
          <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              Tania Andrew
              <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li>
                  <a href="#">My Profile</a>
                </li>
                <li>
                  <a href="#">Edit Profile</a>
                </li>
                <li>
                  <a href="#">Settings</a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <ul class="nav">
          <li>
            <a href="../new_dashboard.php">
              <i class="pe-7s-graph"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="./products.php">
              <i class="pe-7s-plugin"></i>
              <p>Products
              </p>
            </a>
          </li>

          <li>
            <a href="./user.php">
              <i class="pe-7s-user"></i>
              <p>User
              </p>
            </a>
          </li>
          <li class="active">
            <a href="./order.php">
              <i class="pe-7s-cart"></i>
              <p>Order
              </p>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
              <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
              <i class="fa fa-navicon visible-on-sidebar-mini"></i>
            </button>
          </div>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Dashboard PRO</a>
          </div>
          <div class="collapse navbar-collapse">

            <form class="navbar-form navbar-left navbar-search-form" role="search">
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-search"></i>
                </span>
                <input type="text" value="" class="form-control" placeholder="Search...">
              </div>
            </form>

            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="charts.html">
                  <i class="fa fa-line-chart"></i>
                  <p>Stats</p>
                </a>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-gavel"></i>
                  <p class="hidden-md hidden-lg">
                    Actions
                    <b class="caret"></b>
                  </p>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Create New Post</a>
                  </li>
                  <li>
                    <a href="#">Manage Something</a>
                  </li>
                  <li>
                    <a href="#">Do Nothing</a>
                  </li>
                  <li>
                    <a href="#">Submit to live</a>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <a href="#">Another Action</a>
                  </li>
                </ul>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="notification">5</span>
                  <p class="hidden-md hidden-lg">
                    Notifications
                    <b class="caret"></b>
                  </p>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Notification 1</a>
                  </li>
                  <li>
                    <a href="#">Notification 2</a>
                  </li>
                  <li>
                    <a href="#">Notification 3</a>
                  </li>
                  <li>
                    <a href="#">Notification 4</a>
                  </li>
                  <li>
                    <a href="#">Another notification</a>
                  </li>
                </ul>
              </li>

              <li class="dropdown dropdown-with-icons">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-list"></i>
                  <p class="hidden-md hidden-lg">
                    More
                    <b class="caret"></b>
                  </p>
                </a>
                <ul class="dropdown-menu dropdown-with-icons">
                  <li>
                    <a href="#">
                      <i class="pe-7s-mail"></i> Messages
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="pe-7s-help1"></i> Help Center
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="pe-7s-tools"></i> Settings
                    </a>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <a href="#">
                      <i class="pe-7s-lock"></i> Lock Screen
                    </a>
                  </li>
                  <li>
                    <a href="#" class="text-danger">
                      <i class="pe-7s-close-circle"></i>
                      Log out
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-md-12">
              <div class="card">

                <div class="toolbar">

                  <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>

                <table id="bootstrap-table" class="table">

                  <thead>
                    <!-- <th data-field="state" data-checkbox="true"></th> -->
                    <th data-field="id" class="text-center">ID</th>
                    <th data-field="nama_pembeli" data-sortable="true">Nama PEMBELI</th>
                    <th data-field="toko" data-sortable="true">Toko</th>
                    <th data-field="barang" data-sortable="true">Barang</th>
                    <th data-field="tanggal" data-sortable="true">Tanggal</th>
                    <th data-field="jumlah" data-sortable="true">Jumlah</th>
                    <th data-field="total" data-sortable="true">Total</th>
                    <th data-field="status" data-sortable="true">Status</th>
                    <!-- <th data-field="city"></th> -->
                    <th data-field="actions" class="td-actions text-right" data-events="operateEvents" data-formatter="operateFormatter">AKSI</th>
                  </thead>
                  <tbody>
                    <?php 
                    if(mysqli_num_rows($res) > 0){
                      $i = 0;
                      while($row = mysqli_fetch_assoc($res)){?>
                      <tr id ="row">
                        <!-- <td></td> -->
                        <td><?php echo $row["id"] ?></td>
                        <td><?php echo $row["name"] ?></td>
                        <td><?php echo $row["shop_name"] ?></td>
                        <td><?php echo $row["nama"] ?></td>
                        <td><?php echo $row["created_at"] ?></td>
                        <td><?php echo $row["quantity"] ?></td>
                        <td><?php echo ($row["quantity"] * $row["harga"]) ?></td>
                        <td id="<?php echo "status" . $i ?>"><?php echo $row["status_order"] ?></td>
                        <?php $i += 1;?>
                      </tr>
                    <?php }}?>
                  </tbody>
                </table>
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->

        </div>
      </div>

      <footer class="footer">
        <div class="container-fluid">
          <nav class="pull-left">

          </nav>
          <p class="copyright pull-right">
            &copy; 2018
            <a href="#">Creative Tim</a>, dibuat dengan Semangat Pemuda
          </p>
        </div>
      </footer>

    </div>
  </div>


</body>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="../assets/js/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="../assets/js/jquery.validate.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="../assets/js/moment.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="../assets/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="../assets/js/bootstrap-selectpicker.js"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="../assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="../assets/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="../assets/js/jquery-jvectormap.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
<script src="../assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="../assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="../assets/js/jquery.datatables.js"></script>


<!--  Full Calendar Plugin    -->
<script src="../assets/js/fullcalendar.min.js"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="../assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>

<script type="text/javascript">
  $(document).ready(function () {

    demo.initDashboardPageCharts();
    demo.initVectorMap();

    $.notify({
      icon: 'pe-7s-bell',
      message: "<b>Light Bootstrap Dashboard PRO</b> - forget about boring dashboards."

    }, {
      type: 'warning',
      timer: 4000
    });


  });
  var $table = $('#bootstrap-table');

  function operateFormatter(value, row, index) {
    return [
      '<a rel="tooltip" title="Tahap selanjutnya" class="btn btn-simple btn-info btn-icon table-action selanjutnya" href="javascript:void(0)">',
      '<i class="fa fa-plus"></i>',
      '</a>',
      '<a rel="tooltip" title="Tahap sebelumnya" class="btn btn-simple btn-warning btn-icon table-action sebelumnya" href="javascript:void(0)">',
      '<i class="fa fa-minus"></i>',
      '</a>',
    ].join('');
  }

  $().ready(function () {

    let status = [
      'Pesanan diterima',
      'Packing',
      'Sedang diantar',
      'Sampai',
      'Telah dibayar'
    ]

    window.operateEvents = {
      'click .selanjutnya': function (e, value, row, index) {
        var length = <?php echo $i;?>;
        console.log(length)
        console.log(row)
        for (let i = 0; i < length ; i++) {
          if (i === index) {
            if (row.status === 'Pesanan diterima' || row.status === '') {
              $(`#status${index}`).html(status[1])
              row.status = status[1]
            } else if (row.status === 'Packing') {
              $(`#status${index}`).html(status[2])
              row.status = status[2]
            } else if (row.status === 'Sedang diantar') {
              $(`#status${index}`).html(status[3])
              row.status = status[3]
            } else if (row.status === 'Sampai') {
              $(`#status${index}`).html(status[4])
              row.status = status[4]
            }
          }
        }
        console.log(row.id)
        console.log(row.status)        
        $.ajax({
          url: './updatestatus.php',
          type: 'GET',
          data: {
            id: row.id,
            status: row.status
          },
          success: function(result){
            console.log("Berhasil" + result)
          }
        })
      },
      'click .sebelumnya': function (e, value, row, index) {
        var length = <?php echo $i;?>;
        for (let i = 0; i < length; i++) {
          if (i === index) {
            if (row.status === 'Packing') {
              $(`#status${index}`).html(status[0])
              row.status = status[0]
            } else if (row.status === 'Sedang diantar') {
              $(`#status${index}`).html(status[1])
              row.status = status[1]
            } else if (row.status === 'Sampai') {
              $(`#status${index}`).html(status[2])
              row.status = status[2]
            } else if (row.status === 'Telah dibayar') {
              $(`#status${index}`).html(status[3])
              row.status = status[3]
            }
          }
        }
        $.ajax({
          url: './updatestatus.php',
          type: 'GET',
          data: {
            id: row.id,
            status: row.status
          },
          success: function(result){
            console.log("Berhasil" + result)
          }
        })
        console.log(row.status)
      },
    };

    $table.bootstrapTable({
      toolbar: ".toolbar",
      clickToSelect: true,
      showRefresh: true,
      search: true,
      showToggle: true,
      showColumns: true,
      pagination: true,
      searchAlign: 'left',
      pageSize: 8,
      clickToSelect: false,
      pageList: [8, 10, 25, 50, 100],

      formatShowingRows: function (pageFrom, pageTo, totalRows) {
        //do nothing here, we don't want to show the text "showing x of y from..."
      },
      formatRecordsPerPage: function (pageNumber) {
        return pageNumber + " rows visible";
      },
      icons: {
        refresh: 'fa fa-refresh',
        toggle: 'fa fa-th-list',
        columns: 'fa fa-columns',
        detailOpen: 'fa fa-plus-circle',
        detailClose: 'fa fa-minus-circle'
      }
    });

    //activate the tooltips after the data table is initialized
    $('[rel="tooltip"]').tooltip();

    $(window).resize(function () {
      $table.bootstrapTable('resetView');
    });


  });
</script>

</html>