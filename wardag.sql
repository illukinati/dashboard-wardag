-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 06 Jul 2018 pada 18.59
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wardag`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `Admins`
--

CREATE TABLE `Admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `Admins`
--

INSERT INTO `Admins` (`id`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin@warungdaging.com', '$2b$12$M5OP2hl56LR6cqUA87.WxukFX0gHPs58j4vbM4iwsHnIMAp97Gc9q', '2018-07-02 05:57:36', '2018-07-02 05:57:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `address` varchar(128) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone_number`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Budi Man', '081213141516', 'Jl.Kemanamana', '2018-05-10 15:09:59', '2018-05-10 15:39:49'),
(2, 'Ma Man', '086151413121', 'Jln. Brlbng', '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(5, 'Ananta Pratama', '081330747579', 'Jl. Kenangan No. 1', '2018-05-10 15:49:15', '2018-05-10 15:49:15'),
(6, 'Anonymous', '08123456789', 'Nowhere', '2018-05-10 16:17:43', '2018-05-10 16:17:43'),
(7, 'hayria', '08564555', 'diniyo', '2018-05-11 03:54:12', '2018-05-11 03:54:12'),
(8, 'rendra', '081382755387', 'mampang 11 no 67 A', '2018-05-11 03:59:15', '2018-05-11 03:59:15'),
(9, 'dhea n', '082299485055', 'dhnffrkj jhfjknb', '2018-05-11 04:02:02', '2018-05-11 04:02:02'),
(10, 'dhea', '082299485055', 'hduwkwnen', '2018-05-11 04:19:09', '2018-05-11 04:19:09'),
(11, 'Ananta Pratama', '0817758488584', 'Jl. Kenangan Indah No. 1', '2018-06-04 01:42:58', '2018-06-04 01:42:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `partner_id` int(10) UNSIGNED NOT NULL,
  `shop_name` varchar(30) NOT NULL,
  `shipment_address` varchar(255) NOT NULL,
  `shipment_latitude` double NOT NULL,
  `shipment_longitude` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status_order` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `partner_id`, `shop_name`, `shipment_address`, `shipment_latitude`, `shipment_longitude`, `created_at`, `updated_at`, `status_order`) VALUES
(1, 1, 1, 'PT. Daging Makmur', 'Jl.Veteran No.2', -7.9599735, 112.613881, '2018-05-10 15:09:59', '2018-05-10 15:09:59', 'Telah dibayar'),
(2, 1, 2, 'PT. Daging Makmur', 'Jl.Veteran No.2', -7.9599735, 112.613881, '2018-05-10 15:09:59', '2018-05-10 15:09:59', 'Sampai'),
(3, 2, 2, 'PT. Daging Makmur', 'Jl.Soekarno Hatta No.25', -7.9423664, 112.6203379, '2018-05-10 15:09:59', '2018-05-10 15:09:59', 'Sedang diantar'),
(4, 5, 1, 'PT. Daging Makmur', 'Jl.Veteran No.1', -7.9599735, 112.613881, '2018-05-10 15:55:13', '2018-05-10 15:55:13', 'Packing'),
(5, 5, 1, 'PT. Daging Makmur', 'Jl. Kenangan No. 1', -7.9599735, 112.613881, '2018-05-10 16:13:36', '2018-05-10 16:13:36', 'Telah dibayar'),
(6, 6, 1, 'PT. Daging Makmur', 'Jl. Disana No. 9', -7.9599735, 112.613881, '2018-05-10 16:26:04', '2018-05-10 16:26:04', 'Sedang diantar'),
(7, 5, 1, 'PT. Daging Makmur', 'Jl. Disini No. 1', -7.9599735, 112.613881, '2018-05-10 16:28:51', '2018-05-10 16:28:51', 'Telah dibayar'),
(8, 7, 1, '', 'jskalaa', -7.9599735, 112.613881, '2018-05-11 03:57:16', '2018-05-11 03:57:16', 'Telah dibayar'),
(9, 6, 1, '', 'graha asa', -7.9599735, 112.613881, '2018-05-11 03:57:17', '2018-05-11 03:57:17', ''),
(10, 6, 1, '', 'Nowhere', -7.9599735, 112.613881, '2018-05-11 03:57:24', '2018-05-11 03:57:24', ''),
(11, 8, 1, '', 'mampang 11 no 67 A', -7.9599735, 112.613881, '2018-05-11 04:00:03', '2018-05-11 04:00:03', ''),
(12, 6, 1, '', 'huddd', -7.9599735, 112.613881, '2018-05-11 04:00:18', '2018-05-11 04:00:18', ''),
(13, 8, 1, '', 'mampang 11 no 67 A', -7.9599735, 112.613881, '2018-05-11 04:14:33', '2018-05-11 04:14:33', ''),
(14, 10, 1, '', 'hduwkwnen', -7.9599735, 112.613881, '2018-05-11 07:38:57', '2018-05-11 07:38:57', ''),
(15, 6, 1, '', 'Jl. kenangan', -7.9599735, 112.613881, '2018-05-20 13:41:04', '2018-05-20 13:41:04', ''),
(16, NULL, 1, '', 'Jl. Beo 55', -7.9599735, 112.613881, '2018-06-06 08:01:35', '2018-06-06 08:01:35', ''),
(17, 11, 1, '', 'Jl. Kenangan Indah No. 1', -7.9599735, 112.613881, '2018-06-27 03:57:12', '2018-06-27 03:57:12', ''),
(18, 11, 1, '', 'Jl. Kenangan Indah No. 1', -7.9599735, 112.613881, '2018-06-30 04:22:26', '2018-06-30 04:22:26', ''),
(19, 6, 1, '', 'Terusan Sigura-gura No. 7', -7.9599735, 112.613881, '2018-06-30 06:31:33', '2018-06-30 06:31:33', ''),
(20, 6, 1, '', 'Malang', -7.9599735, 112.613881, '2018-06-30 06:33:32', '2018-06-30 06:33:32', ''),
(21, 6, 1, '', 'Jakarta Barat', -7.9599735, 112.613881, '2018-06-30 07:09:42', '2018-06-30 07:09:42', ''),
(22, 6, 1, '', 'Jawa Timur', -7.9599735, 112.613881, '2018-06-30 07:14:34', '2018-06-30 07:14:34', ''),
(23, 6, 1, '', 'Jogja', -7.9599735, 112.613881, '2018-06-30 07:15:58', '2018-06-30 07:15:58', ''),
(24, 6, 1, '', 'Champion', -7.9599735, 112.613881, '2018-06-30 07:22:25', '2018-06-30 07:22:25', ''),
(25, 6, 1, '', 'Nowhere', -7.9599735, 112.613881, '2018-06-30 17:37:14', '2018-06-30 17:37:14', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_items`
--

CREATE TABLE `order_items` (
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_items`
--

INSERT INTO `order_items` (`order_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 15, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(1, 3, 19, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(2, 3, 3, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 1, 37, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 2, 7, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 3, 27, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(4, 1, 1, '2018-05-10 15:55:13', '2018-05-10 15:55:13'),
(4, 2, 1, '2018-05-10 15:55:13', '2018-05-10 15:55:13'),
(5, 1, 1, '2018-05-10 16:13:36', '2018-05-10 16:13:36'),
(6, 1, 1, '2018-05-10 16:26:04', '2018-05-10 16:26:04'),
(7, 1, 1, '2018-05-10 16:28:51', '2018-05-10 16:28:51'),
(8, 1, 2, '2018-05-11 03:57:16', '2018-05-11 03:57:16'),
(8, 2, 1, '2018-05-11 03:57:16', '2018-05-11 03:57:16'),
(9, 1, 1, '2018-05-11 03:57:17', '2018-05-11 03:57:17'),
(10, 1, 1, '2018-05-11 03:57:24', '2018-05-11 03:57:24'),
(11, 1, 1, '2018-05-11 04:00:03', '2018-05-11 04:00:03'),
(12, 1, 2, '2018-05-11 04:00:18', '2018-05-11 04:00:18'),
(12, 2, 1, '2018-05-11 04:00:18', '2018-05-11 04:00:18'),
(13, 2, 1, '2018-05-11 04:14:33', '2018-05-11 04:14:33'),
(14, 1, 1, '2018-05-11 07:38:57', '2018-05-11 07:38:57'),
(14, 2, 1, '2018-05-11 07:38:57', '2018-05-11 07:38:57'),
(15, 1, 1, '2018-05-20 13:41:04', '2018-05-20 13:41:04'),
(16, 1, 1, '2018-06-06 08:01:35', '2018-06-06 08:01:35'),
(17, 3, 2, '2018-06-27 03:57:13', '2018-06-27 03:57:13'),
(18, 3, 1, '2018-06-30 04:22:27', '2018-06-30 04:22:27'),
(19, 2, 2, '2018-06-30 06:31:33', '2018-06-30 06:31:33'),
(20, 2, 3, '2018-06-30 06:33:32', '2018-06-30 06:33:32'),
(21, 3, 3, '2018-06-30 07:09:42', '2018-06-30 07:09:42'),
(22, 3, 1, '2018-06-30 07:14:34', '2018-06-30 07:14:34'),
(23, 3, 2, '2018-06-30 07:15:58', '2018-06-30 07:15:58'),
(24, 3, 2, '2018-06-30 07:22:25', '2018-06-30 07:22:25'),
(25, 1, 2, '2018-06-30 17:37:14', '2018-06-30 17:37:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `address` varchar(128) NOT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `rating` float UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `status` enum('PENDING','APPROVED') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `partners`
--

INSERT INTO `partners` (`id`, `name`, `address`, `latitude`, `longitude`, `rating`, `user_id`, `created_at`, `updated_at`, `nik`, `status`) VALUES
(1, 'Tukiman', 'Jl.Kawi Atas No.38', -7.9750743, 112.6174305, 4.5, 3, '2018-05-10 15:09:59', '2018-05-10 15:09:59', NULL, 'PENDING'),
(2, 'Teman', 'Jl.Raya Langsep No.2', -7.9717287, 112.6129617, 1.775, 4, '2018-05-10 15:09:59', '2018-05-10 15:09:59', NULL, 'PENDING');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(128) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `harga` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gambar` varchar(40) NOT NULL,
  `love_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `kategori` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `nama`, `deskripsi`, `harga`, `gambar`, `love_count`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'Daging Kambing yo aaa', 'Daging rasa ayam', 40000, 'uploads/dagingkambing.jpeg', 15, 'Kambing', '2018-05-10 15:09:59', '2018-07-05 19:26:47'),
(2, 'Daging Kambing', 'Daging rasa Kambing', 25000, '', 157, 'Kambing', '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 'Daging Sapi', 'Daging rasa Sapi', 80000, '', 12637, 'Sapi', '2018-05-10 15:09:59', '2018-05-10 15:09:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_stocks`
--

CREATE TABLE `product_stocks` (
  `product_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `partner_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `stock` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_stocks`
--

INSERT INTO `product_stocks` (`product_id`, `partner_id`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 167, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(1, 2, 82, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(2, 1, 321, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(2, 2, 222, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 1, 71, '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 2, 911, '2018-05-10 15:09:59', '2018-05-10 15:09:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `SequelizeMeta`
--

CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `SequelizeMeta`
--

INSERT INTO `SequelizeMeta` (`name`) VALUES
('20180505091919-user.js'),
('20180505132327-customer.js'),
('20180505132337-partner.js'),
('20180505132358-product.js'),
('20180505132409-product-stock.js'),
('20180505132415-order.js'),
('20180505132417-order-item.js'),
('20180606040718-update-order-table.js'),
('20180606041530-add-nik-and-status-in-partner.js'),
('20180629145930-create-admin.js');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `peran` varchar(10) NOT NULL,
  `password_reset_token` varchar(128) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT '10',
  `profile_picture` varchar(128) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `email`, `nama`, `username`, `password`, `phone`, `alamat`, `peran`, `password_reset_token`, `auth_key`, `status`, `profile_picture`, `created_at`, `updated_at`) VALUES
(1, 'budiman@mail.com', 'Budi Mantuls', 'budiman', '$2b$12$O//hLWEBInXm3uCSZdtq6ecJjjoSpyfukjne7dAEWdwmeKYYBX3WO', '081234892314', 'Jalan Simpangan', 'User', NULL, NULL, 10, '', '2018-05-10 15:09:59', '0000-00-00 00:00:00'),
(2, 'maman@mail.com', '', 'maman', '$2b$12$O//hLWEBInXm3uCSZdtq6ecJjjoSpyfukjne7dAEWdwmeKYYBX3WO', '081234892314', 'Jalan Simpangan', 'Mitra', NULL, NULL, 10, '', '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(3, 'tukiman@mail.com', '', 'tukiman', '$2b$12$O//hLWEBInXm3uCSZdtq6ecJjjoSpyfukjne7dAEWdwmeKYYBX3WO', '081234892314', 'Jalan Simpangan', 'User', NULL, NULL, 10, '', '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(4, 'teman@mail.com', '', 'teman', '$2b$12$O//hLWEBInXm3uCSZdtq6ecJjjoSpyfukjne7dAEWdwmeKYYBX3WO', '081234892314', 'Jalan Simpangan', 'Mitra', NULL, NULL, 10, '', '2018-05-10 15:09:59', '2018-05-10 15:09:59'),
(5, 'hujan@mail.com', '', 'hujan@mail.com', '$2b$12$q5GSEYKj8ys/JgZHueIQaOB6Lz0HyrH/Xas51iHB9UJW4WTzMbBpe', '081234892314', 'Jalan Simpangan', 'User', NULL, 'inprogress', 10, NULL, '2018-05-10 15:49:15', '2018-05-10 15:49:15'),
(6, 'anonymous@mail.com', '', 'anonymous@mail.com', '$2b$12$f5zUUBHLKj5MWEOmN92J1OdQmMtzQkMqKZWY8mhm7x6NLITgt8MMO', '081234892314', 'Jalan Simpangan', 'User', NULL, 'inprogress', 10, NULL, '2018-05-10 16:17:43', '2018-05-10 16:17:43'),
(7, 'hayria76@gmail.com', '', 'hayria76@gmail.com', '$2b$12$OwIEeL1npOZA.PeFGmZXU./Ww9aYjzAFiec0gLgOVCcSnT/MfLhYi', '081234892314', 'Jalan Simpangan', 'User', NULL, 'inprogress', 10, NULL, '2018-05-11 03:54:12', '2018-05-11 03:54:12'),
(8, 'rendra.kurniawan2@gmail.com', '', 'rendra.kurniawan2@gmail.com', '$2b$12$DzLnX2x7ZyWqGX68CY0AM.mmCt4JdTRBOjveCENd4.hFQha4oN60O', '081234892314', 'Jalan Simpangan', 'Mitra', NULL, 'inprogress', 10, NULL, '2018-05-11 03:59:15', '2018-05-11 03:59:15'),
(9, 'dhe.novper@gmail.com', '', 'dhe.novper@gmail.com', '$2b$12$imn7ahxytiCqCnchxR2Zv.MIk7wy2GBHZ/09qx8fo3GAMDBg1xrqC', '081234892314', 'Jalan Simpangan', 'Mitra', NULL, 'inprogress', 10, NULL, '2018-05-11 04:02:02', '2018-05-11 04:02:02'),
(10, 'dhea.novanda@gmail.com', '', 'dhea.novanda@gmail.com', '$2b$12$OwIEeL1npOZA.PeFGmZXU./Ww9aYjzAFiec0gLgOVCcSnT/MfLhYi', '081234892314', 'Jalan Simpangan', 'Mitra', NULL, 'inprogress', 10, NULL, '2018-05-11 04:19:09', '2018-05-11 04:19:09'),
(11, 'deras.hujan15@gmail.com', '', 'deras.hujan15@gmail.com', '$2b$12$gnHVusGcW4yOi20oAWgBEeH8cIGvKzecOWPvRIWZW2pHHDDzbjpX2', '081234892314', 'Jalan Simpangan', 'User', NULL, 'inprogress', 10, NULL, '2018-06-04 01:42:58', '2018-06-04 01:42:58'),
(16, 'okza123@gmail.com', 'Okza', 'okzapurda', '', '08123456789', 'Jalan Sesama', 'User', NULL, NULL, 10, NULL, '2018-07-06 17:52:37', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Admins`
--
ALTER TABLE `Admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `partner_id` (`partner_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`order_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`product_id`,`partner_id`),
  ADD KEY `partner_id` (`partner_id`);

--
-- Indexes for table `SequelizeMeta`
--
ALTER TABLE `SequelizeMeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Admins`
--
ALTER TABLE `Admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`);

--
-- Ketidakleluasaan untuk tabel `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Ketidakleluasaan untuk tabel `partners`
--
ALTER TABLE `partners`
  ADD CONSTRAINT `partners_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD CONSTRAINT `product_stocks_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `product_stocks_ibfk_2` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
