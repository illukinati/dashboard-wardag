<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Warung Daging</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>

<body>

    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="assets/img/full-screen-image-3.jpg">
            <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

            <div class="logo">
                <a href="http://www.creative-tim.com" class="logo-text">
                    Creative Tim
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="http://www.creative-tim.com" class="logo-text">
                    Ct
                </a>
            </div>

            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="assets/img/default-avatar.png" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            Tania Andrew
                            <b class="caret"></b>
                        </a>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="#">My Profile</a>
                                </li>
                                <li>
                                    <a href="#">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <ul class="nav">
                    <li class="active">
                        <a href="#">
                            <i class="pe-7s-graph"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="./dashboard-wardag/products.php">
                            <i class="pe-7s-plugin"></i>
                            <p>Products
                            </p>
                        </a>
                    </li>

                    <li>
                        <a href="./dashboard-wardag/user.php">
                            <i class="pe-7s-user"></i>
                            <p>User
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="./dashboard-wardag/order.php">
                            <i class="pe-7s-cart"></i>
                            <p>Order
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
                            <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                            <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Dashboard PRO</a>
                    </div>
                    <div class="collapse navbar-collapse">

                        <form class="navbar-form navbar-left navbar-search-form" role="search">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" value="" class="form-control" placeholder="Search...">
                            </div>
                        </form>

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="charts.html">
                                    <i class="fa fa-line-chart"></i>
                                    <p>Stats</p>
                                </a>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-gavel"></i>
                                    <p class="hidden-md hidden-lg">
                                        Actions
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Create New Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Manage Something</a>
                                    </li>
                                    <li>
                                        <a href="#">Do Nothing</a>
                                    </li>
                                    <li>
                                        <a href="#">Submit to live</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">Another Action</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="notification">5</span>
                                    <p class="hidden-md hidden-lg">
                                        Notifications
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Notification 1</a>
                                    </li>
                                    <li>
                                        <a href="#">Notification 2</a>
                                    </li>
                                    <li>
                                        <a href="#">Notification 3</a>
                                    </li>
                                    <li>
                                        <a href="#">Notification 4</a>
                                    </li>
                                    <li>
                                        <a href="#">Another notification</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-with-icons">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-list"></i>
                                    <p class="hidden-md hidden-lg">
                                        More
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu dropdown-with-icons">
                                    <li>
                                        <a href="#">
                                            <i class="pe-7s-mail"></i> Messages
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="pe-7s-help1"></i> Help Center
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="pe-7s-tools"></i> Settings
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <i class="pe-7s-lock"></i> Lock Screen
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-danger">
                                            <i class="pe-7s-close-circle"></i>
                                            Log out
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>

            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div class="header">
                                    <h4 class="title">Global Sales</h4>
                                </div>
                                <div class="content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <p class="category">Top five Product</p>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>No</td>
                                                            <td>Jenis</td>
                                                            <td class="text-right">
                                                                Today
                                                            </td>
                                                            <td class="text-right">
                                                                Weekly
                                                            </td>
                                                            <td class="text-right">
                                                                Monthly
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Sapi</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                12.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Ayam</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                12.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Kambing</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                12.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Olahan</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                12.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>Lain</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                12.3 Kg
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <p class="category">Top Five Mitra</p>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>No</td>
                                                            <td>Nama</td>
                                                            <td class="text-right">
                                                                Today
                                                            </td>
                                                            <td class="text-right">
                                                                Weekly
                                                            </td>
                                                            <td class="text-right">
                                                                Monthly
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>User 1</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                                <img src="http://icons.iconarchive.com/icons/graphicloads/100-flat-2/16/arrow-up-icon.png">
                                                            </td>
                                                            <td class="text-right">
                                                                3.3 Kg
                                                                <img src="https://www.shareicon.net/data/16x16/2015/09/20/104246_down_256x256.png">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>User 2</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                3.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>User 3</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                3.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>User 4</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                3.3 Kg
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>User 5</td>
                                                            <td class="text-right">
                                                                1.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                2.3 Kg
                                                            </td>
                                                            <td class="text-right">
                                                                3.3 Kg
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Pembelian </h4>
                                    <p class="category">Pembelian Daging 7 Hari terakhir</p>
                                </div>
                                <div class="content">
                                    <div id="chartEmail" class="ct-chart "></div>
                                </div>
                                <div class="footer">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Ayam
                                        <i class="fa fa-circle text-danger"></i> Sapi
                                        <i class="fa fa-circle text-warning"></i> Kambing
                                    </div>
                                    <hr>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Users Pembelian</h4>
                                    <p class="category">24 Hours performance</p>
                                </div>
                                <div class="content">
                                    <div id="chartHours" class="ct-chart"></div>
                                </div>
                                <div class="footer">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Ayam
                                        <i class="fa fa-circle text-danger"></i> Sapi
                                        <i class="fa fa-circle text-warning"></i> Kambing
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6">
                            <div class="card ">
                                <div class="header">
                                    <h4 class="title">2018 Penjualan</h4>
                                    <p class="category">All products including Taxes</p>
                                </div>
                                <div class="content">
                                    <div id="chartActivity" class="ct-chart"></div>
                                </div>
                                <div class="footer">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Ayam
                                        <i class="fa fa-circle text-danger"></i> Kambing
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card ">
                                <div class="header">
                                    <h4 class="title">Tasks</h4>
                                    <p class="category">Pembelian Terakhir</p>
                                </div>
                                <div class="content">
                                    <div class="table-full-width">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="" data-toggle="checkbox">
                                                        </label>
                                                    </td>
                                                    <td>User 1 Baru Beli 5kg Daging Kambing"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="" data-toggle="checkbox" checked="">
                                                        </label>
                                                    </td>
                                                    <td>User 1 Baru Beli 5kg Daging Ayam"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="" data-toggle="checkbox" checked="">
                                                        </label>
                                                    </td>
                                                    <td>User 21 Baru Beli 5kg Daging Kambing"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="" data-toggle="checkbox">
                                                        </label>
                                                    </td>
                                                    <td>User 4 Baru Beli 5kg Daging Kambing"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="" data-toggle="checkbox">
                                                        </label>
                                                    </td>
                                                    <td>User 5 Baru Beli 5kg Daging Kambing"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="" data-toggle="checkbox">
                                                        </label>
                                                    </td>
                                                    <td>User 6 Baru Beli 5kg Daging Kambing"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>



                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                  <nav class="pull-left">
        
                  </nav>
                  <p class="copyright pull-right">
                    &copy; 2018
                    <a href="#">Creative Tim</a>, dibuat dengan Semangat Pemuda
                  </p>
                </div>
              </footer>

        </div>
    </div>


</body>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="assets/js/jquery.validate.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="assets/js/moment.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="assets/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="assets/js/bootstrap-selectpicker.js"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="assets/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="assets/js/jquery-jvectormap.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
<script src="assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="assets/js/jquery.datatables.js"></script>


<!--  Full Calendar Plugin    -->
<script src="assets/js/fullcalendar.min.js"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        demo.initDashboardPageCharts();
        demo.initVectorMap();

        $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Light Bootstrap Dashboard PRO</b> - forget about boring dashboards."

        }, {
            type: 'warning',
            timer: 4000
        });

    });
</script>

</html>